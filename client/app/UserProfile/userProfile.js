Session.set("showPasswordForm", false);

Template.userProfile.helpers({
    email: function () {                
        return  Meteor.user() && Meteor.user().emails ? Meteor.user().emails[0].address : "";      
    },
    name: function () {        
        return  Meteor.user() &&  Meteor.user().profile ? Meteor.user().profile.name : "";      
    },
    location: function () {        
        return  Meteor.user() &&  Meteor.user().profile ? Meteor.user().profile.location : "";      
    },
    showPasswordForm: function () {
      return Session.get("showPasswordForm");
    },
    locations: function () {
        return Locations.find();      
    } 
}); 

Template.userProfile.events({
    "submit .save-profile": function (event) {      
      event.preventDefault();
      
      var name = event.target.name.value;
      var oldPassword = event.target.oldPassword ?  event.target.oldPassword.value : null;       
      var newPassword = event.target.newPassword ? event.target.newPassword.value : null;
      var location =  event.target.location.value;
      
            
      Meteor.users.update(Meteor.userId(), {$set: {
          profile: { 
              name: name,
              location:  location
          }
      }});     
      
      if (oldPassword && newPassword) {
        Accounts.changePassword(oldPassword, newPassword);
      }
      
      $('#userProfileDialog').modal('hide');                   
    },
    "change .show-password-form input": function (event) {              
       Session.set("showPasswordForm", event.target.checked);                    
    }
 }); 

