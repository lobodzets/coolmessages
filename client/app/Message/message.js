Template.message.helpers({
    location: function () {
		var location = Locations.findOne({_id: new Meteor.Collection.ObjectID( this.location) });		
        return location ? location.name : '';  
    },
	ownerName: function () {
		var owner = Meteor.users.findOne({_id: this.owner });				
        return owner && owner.profile ? owner.profile.name : '';  
    }
}); 