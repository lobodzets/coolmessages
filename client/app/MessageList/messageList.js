Template.messageList.helpers({
    messages: function () {
        return Meteor.user() && Meteor.user().profile ? Messages.find({ location: Meteor.user().profile.location }) : [];      
    }
}); 

Template.messageList.events({
    "submit .new-message": function (event) {      
      event.preventDefault();
                    
      Messages.insert({
        text: event.target.text.value,
        createdAt: new Date(),
        owner: Meteor.userId(),   
        location: Meteor.user().profile.location     
      });
 
      event.target.text.value = "";
    }
 }); 
 
 