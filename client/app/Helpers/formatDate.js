Template.registerHelper('formatDate', function(date) {
  //return date.toISOString().substring(0, 10);
  
  return  [
      date.getDate(),
      date.getMonth()+1,      
      date.getFullYear()
    ].join('/') + ' ' +  [
      date.getHours(),
      date.getMinutes(),
      date.getSeconds()
    ].join(':');
});